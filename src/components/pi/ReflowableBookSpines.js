import React from "react"
import ReactList from "react-list"

import Book from "../book"
import "../../styles/ReflowableBookSpines.css"

const BOUGHT_BOOKS = [2, 3, 4, 5]

const renderPiBookSpines = (index) => (
  <Book key={index + 1} volume={index + 1} bookIsBought={BOUGHT_BOOKS.includes(index + 1)} />
)

const ReflowableBookSpines = () => (
  <div className="reactListWrapper">
    <ReactList itemRenderer={renderPiBookSpines} length={1000} type="uniform" />
  </div>
)

export default ReflowableBookSpines
