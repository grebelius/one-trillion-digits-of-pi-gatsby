import React from "react"
import bookData from "./bookData"

const totalNumberOfCards = Math.ceil(bookData.pages.total / 2)

const Slider = ({ currentRightmostCard }) => (
  <input
    type="range"
    min="1"
    max={totalNumberOfCards}
    value={currentRightmostCard}
    className="slider"
  />
)

export default Slider
