import React from "react"

import PictureTag from "./PictureTag"

const Card = ({
  onClick,
  vol,
  cardNumber,
  position,
  totalNumberOfCards,
  bookData,
  flipDirection,
  downloadedImagesCallback,
  shouldDownloadImmediatly,
}) => {
  const frontPageNumber = cardNumber * 2 - 1
  const backPageNumber = cardNumber * 2

  const [shouldDownloadImage, setShouldDownloadImage] = React.useState(false)

  React.useEffect(() => {
    let lazyImageTimeout
    if (shouldDownloadImmediatly) {
      setShouldDownloadImage(true)
    } else {
      lazyImageTimeout = setTimeout(() => {
        setShouldDownloadImage(true)
        downloadedImagesCallback(cardNumber)
      }, 1000)
    }

    return () => {
      clearTimeout(lazyImageTimeout)
    }
  }, [shouldDownloadImmediatly, downloadedImagesCallback, cardNumber])

  let cardInlineStyle = {
    transitionDuration: `${bookData.pageFlipSpeedInMilliseconds}ms`,
  }

  if (flipDirection === "forwards") {
    switch (position) {
      case 1:
        cardInlineStyle.zIndex = 0
        cardInlineStyle.transform = "rotateY(-180deg)"
        break
      case 2:
        cardInlineStyle.zIndex = 30
        cardInlineStyle.transform = "rotateY(-180deg)"
        break
      case 3:
        cardInlineStyle.zIndex = 10
        cardInlineStyle.transform = "rotateY(0deg)"
        break
      case 4:
        cardInlineStyle.zIndex = 0
        cardInlineStyle.transform = "rotateY(0deg)"
        break
      default:
        console.error("Card components need a position parameter")
    }
  } else {
    switch (position) {
      case 1:
        cardInlineStyle.zIndex = 0
        cardInlineStyle.transform = "rotateY(-180deg)"
        break
      case 2:
        cardInlineStyle.zIndex = 1
        cardInlineStyle.transform = "rotateY(-180deg)"
        break
      case 3:
        cardInlineStyle.zIndex = 2
        cardInlineStyle.transform = "rotateY(0deg)"
        break
      case 4:
        cardInlineStyle.zIndex = 0
        cardInlineStyle.transform = "rotateY(0deg)"
        break
      default:
        console.error("Card components need a position parameter")
    }
  }

  return cardNumber > 0 && cardNumber <= totalNumberOfCards ? (
    <div className="card" style={cardInlineStyle} onClick={() => {onClick(cardNumber)}}>
      <div className="card-face">
        <PictureTag
          vol={vol}
          page={frontPageNumber}
          download={shouldDownloadImage}
        />
      </div>
      <div className="card-face back">
        {backPageNumber > bookData.pages.total ? (
          <div className="empty-page-cover-div" />
        ) : (
          <PictureTag
            vol={vol}
            page={backPageNumber}
            download={shouldDownloadImage}
          />
        )}
      </div>
    </div>
  ) : (
    <></>
  )
}

export default Card
