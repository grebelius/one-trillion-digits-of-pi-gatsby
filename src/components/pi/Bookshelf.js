import React from "react"
import { motion } from "framer-motion"

const variants = {
  hidden: { opacity: 0, x: '100vw' },
  visible: { opacity: 1, x: 0 },
}

const Bookshelf = ({ index }) => (
  <motion.div
    className="bookshelf"
    initial="hidden"
    animate="visible"
    variants={variants}
  >
    <div className="bookshelf-number">{index + 1}</div>
  </motion.div>
)

export default Bookshelf
