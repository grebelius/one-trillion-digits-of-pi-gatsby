import React from "react"

const PictureTag = ({ vol, page, download }) => {
  const basePath = "https://res.cloudinary.com/grebelius/image/upload"
  const transform = `dpr_auto,e_sharpen:100,f_auto,pg_${page},q_auto:best`
  const filePath = `one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-${vol}.pdf`

  return download ? (
    <picture>
      <img
        className="picture"
        sizes="(max-width: 650px) 100vw, 650px"
        srcSet={`${basePath}/${transform},w_120/${filePath} 120w, ${basePath}/${transform},w_650/${filePath} 650w`}
        src={`${basePath}/${transform},w_650/${filePath}`}
        alt={`One Trillion Digits of Pi – Volume ${vol}, page ${page}`}
      />
    </picture>
  ) : (
    <></>
  )
}

export default PictureTag
