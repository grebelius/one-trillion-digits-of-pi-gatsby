import React from "react"
import SEO from "../seo"

import "../../styles/flipbook.css"

import Card from "./Card"
import Label from "./Label"

import bookData from "./bookData"

const Flipbook = ({ vol }) => {
  const [currentRightmostCard, setCurrentRightmostCard] = React.useState(1)
  const [downloadedImages, setDownloadedImages] = React.useState([1, 2])
  // const handleChange = event => {setCurrentRightmostCard(event.target.value)}
  const [flipDirection, setFlipDirection] = React.useState("forwards")
  const totalNumberOfCards = Math.ceil(bookData.pages.total / 2)

  React.useEffect(() => {
    const handleKeyDown = (e) => {
      if (e.keyCode === 39) {
        flipForwardsIntent(1)
      } else if (e.keyCode === 37) {
        flipBackwardsIntent(1)
      }
    }

    document.addEventListener("keyup", handleKeyDown)
    // window.addEventListener("resize", handleWindowResize)

    return function cleanup() {
      document.removeEventListener("keyup", handleKeyDown)
      // window.removeEventListener("resize", handleWindowResize)
    }
  })

  const flipForwardsIntent = (intendedSteps) => {
    setFlipDirection("forwards")
    let diff = totalNumberOfCards + 1 - (currentRightmostCard + intendedSteps)
    if (diff > 0) {
      setCurrentRightmostCard(currentRightmostCard + intendedSteps)
    } else {
      setCurrentRightmostCard(totalNumberOfCards + 1)
    }
  }

  const flipBackwardsIntent = (intendedSteps) => {
    setFlipDirection("backwards")
    let diff = currentRightmostCard - intendedSteps
    if (diff > 0) {
      setCurrentRightmostCard(currentRightmostCard - intendedSteps)
    } else {
      setCurrentRightmostCard(1)
    }
  }

  // -------------------------------------------------------------

  const flipbookInlineStyle = {
    "--bookWidth": bookData.pageDimensions.px.width,
    "--bookHeight": bookData.pageDimensions.px.height,
  }

  // -------------------------------------------------------------
  const cardNumberArray = [
    currentRightmostCard - 2,
    currentRightmostCard - 1,
    currentRightmostCard,
    currentRightmostCard + 1,
  ]

  const memoizedCallback = React.useCallback(
    (cdnum) => {
      setDownloadedImages([...downloadedImages, cdnum])
    },
    [downloadedImages]
  )
  const handleClick = (cardNumber) => {
    if (cardNumber >= currentRightmostCard) {
      flipForwardsIntent(1)
    } else {
      flipBackwardsIntent(1)
    }
  }
  const cards = cardNumberArray.map((cardNumber, index) => (
    <Card
      onClick={handleClick}
      vol={vol}
      cardNumber={cardNumber}
      key={cardNumber}
      totalNumberOfCards={totalNumberOfCards}
      bookData={bookData}
      position={index + 1}
      flipDirection={flipDirection}
      downloadedImagesCallback={memoizedCallback}
      shouldDownloadImmediatly={
        downloadedImages.includes(cardNumber) ? true : false
      }
    />
  ))

  // -------------------------------------------------------------

  return (
    <>
      <SEO title={`Volume ${vol}`} />
      <div className="flipbook" style={flipbookInlineStyle}>
        <div className="scenewrapper">
          <div className="scene">{cards}</div>
        </div>
        <input
          disabled
          type="range"
          min="1"
          max={totalNumberOfCards}
          value={currentRightmostCard}
          className="slider"
        />
        <Label currentRightmostCard={currentRightmostCard} />
      </div>
    </>
  )
}

export default Flipbook
