import React from "react"
import { FixedSizeList as List } from "react-window"
import AutoSizer from "react-virtualized-auto-sizer"
import Bookshelf from "./Bookshelf"

// import Bookshelf from "./Bookshelf"

// const BOUGHT_BOOKS = [2, 3, 4, 5]

const Row = ({ index, isScrolling, style }) => (
  <div style={style}>
    {isScrolling ? "Scrolling" : <Bookshelf index={index} />}
  </div>
)

// class ItemRenderer extends React.PureComponent {
//   render() {
//     return (
//       <div style={this.props.style}>
//         {this.props.isScrolling ? (
//           "Scrolling"
//         ) : (
//           <Bookshelf index={this.props.index} />
//         )}
//       </div>
//     )
//   }
// }

const Test = () => (
  <AutoSizer>
    {({ height, width }) => (
      <List
        useIsScrolling
        height={height}
        itemCount={1000}
        itemSize={100}
        width={width}
        overscanCount={2}
      >
        {Row}
      </List>
    )}
  </AutoSizer>
)
export default Test
