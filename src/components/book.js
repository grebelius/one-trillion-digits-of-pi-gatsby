import React from "react"
import { Link } from "gatsby"

const Book = ({ volume, bookIsBought }) => (
  <Link
    className={`bookSpineLink${bookIsBought ? " bookIsBought" : ""}`}
    to={`/volume/${volume}`}
  >
    <svg fontSize="10" width="50" height="456" xmlns="http://www.w3.org/2000/svg">
      <text
        className="svg-title"
        transform="rotate(90 25,101)"
        y="104"
        x="-52"
      >
        One Trillion Digits of Pi
      </text>
      <text
        className="svg-subtitle"
        transform="rotate(90 25,265)"
        y="268"
        x="-47"
      >
        Volume <tspan className="svg-volume-number">
          {parseFloat(volume).toLocaleString("en")}
        </tspan> of 1,000,000
      </text>
      <text
        className="svg-author"
        transform="rotate(90 25,397)"
        y="400"
        x="-11"
      >
        Lennart Grebelius
      </text>
    </svg>
  </Link>
)

export default Book
