import React from "react"
import { useMatch } from "@reach/router"

import SingleVolumeView from "../../components/pi/SingleVolumeView"

const Volume = () => {
  const match = useMatch("/volume/:vol")
  const volumeNumber = match ? parseInt(match.vol, 10) : null

  return volumeNumber ? <SingleVolumeView vol={volumeNumber} /> : <>Uncool</>
}

export default Volume
