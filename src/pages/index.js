import React from "react"

// import ReflowableBookSpines from "../components/pi/ReflowableBookSpines"
import Test from "../components/pi/Test"

const IndexPage = () => (
  <div className="container-m bookshelves-container">
    <header>
      <h1>One Trillion Digits of Pi</h1>
    </header>
    <main>
      <Test />
    </main>
  </div>
)

export default IndexPage
