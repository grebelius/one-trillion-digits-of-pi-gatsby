// In your routes configuration file
const path = require("path")
module.exports = [
  {
    path: "/:vol",
    component: path.resolve(`src/pages/index.js`),
  },
]
